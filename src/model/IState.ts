import {ILoginState} from '../feature/login/redux/reducers/login.reducer';

export interface IState {
    dispatch: any;
    loginState : ILoginState;
}