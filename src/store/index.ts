import {createBrowserHistory} from 'history';
import {applyMiddleware, combineReducers, compose, createStore, ReducersMapObject, Store} from 'redux';
import {reducer as formReducer} from 'redux-form';
import thunk from 'redux-thunk';
import {loginReducer} from '../feature/login/redux/reducers/login.reducer';
import {IState} from '../model';

const g: any = global;
const composeEnhancers = g.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

const ReducerMap: ReducersMapObject = {};
ReducerMap['form'] = formReducer;
ReducerMap['loginState'] = loginReducer;

const createdStore: Store<IState> = createStore(combineReducers(ReducerMap), enhancer);
export const history = createBrowserHistory();

export const store = createdStore;