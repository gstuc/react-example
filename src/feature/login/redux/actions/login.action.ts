import * as Model from '../../../../model';
import {history} from '../../../../store/index';
import {LoginActionTypes} from './login.action.types';

export const loginLoadingStatus = (flag: boolean) => {
    return {
        type: LoginActionTypes.LOGIN_IS_LOADING,
        payload: flag
    };
};

export const loginSaveUser = (username: string) => {
    return {
        type: LoginActionTypes.SAVE_USERNAME,
        payload: username
    };
};

export const logout = () => {
    return async (dispatch: any, getState: () => Model.IState) => {
        dispatch(loginSaveUser(''));
        history.push('');
    };
};

export const showPassword = (flag: boolean) => {
    return {
        type: LoginActionTypes.SET_PASSWORD_VISIBILITY,
        payload: flag
    };
};

export const login = (username: string, password: string) => {
    return async (dispatch: any) => {
        dispatch(loginLoadingStatus(true));

        await delay(3000);
        if (username === 'admin' && password === 'admin') {
            dispatch(loginSaveUser(username));
            dispatch(loginLoadingStatus(false));
        }
    };
};

function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}