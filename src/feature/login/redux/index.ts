import {
    login, loginSaveUser, loginLoadingStatus, logout, showPassword
} from './actions/login.action';

export const LoginActions = Object.assign({},
    {login: login},
    {loginSaveUser: loginSaveUser},
    {loginLoadingStatus: loginLoadingStatus},
    {logout: logout},
    {showPassword: showPassword}
);

export {loginReducer} from './reducers/login.reducer';