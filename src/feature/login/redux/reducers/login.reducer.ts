import {LoginActionTypes} from '../actions/login.action.types';

export interface ILoginState {
    username: string;
    loginLoading: boolean;
    showPassword: boolean;
}

const initialState: ILoginState = {username: '', loginLoading: false, showPassword: false};

export const loginReducer = (state: ILoginState = initialState, action: any): ILoginState => {
    switch (action.type) {
        case LoginActionTypes.LOGIN_IS_LOADING:
            return Object.assign({}, state, {loginLoading: action.payload});
        case LoginActionTypes.SET_PASSWORD_VISIBILITY:
            return Object.assign({}, state, {showPassword: action.payload});
        case LoginActionTypes.SAVE_USERNAME:
            return Object.assign({}, state, {username: action.payload});
        default:
            return Object.assign({}, state);
    }
};