import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators, Dispatch} from 'redux';
import { reduxForm } from 'redux-form';
import * as Model from '../../../model/index';
import {LoginActions} from '../redux';
import {ILoginState} from '../redux/reducers/login.reducer';


interface ILoginPageProps {
    handleSubmit?: any;
    valid: boolean;
    loginState: ILoginState;
    login: (username: string, password: string, redirectUrl?: string) => void;
    showPassword: (visible: boolean) => void;
    location: any
}

interface IState {
    showError: boolean
}

class Login extends React.Component<ILoginPageProps, IState> {

    constructor(props: ILoginPageProps) {
        super(props);
        this.state = {showError: false};
    };

}

const mapStateToProps = (state: Model.IState) => {
    return {
        loginState: state.loginState
    };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) => {
    return bindActionCreators(Object.assign({}, LoginActions), dispatch);
};

const LoginForm = reduxForm({form: 'login'})(Login);
export const LoginPage = withRouter(connect<ILoginPageProps, any>(mapStateToProps, mapDispatchToProps)(LoginForm));